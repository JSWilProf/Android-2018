package br.senai.sp.informatica.primeiraaplicacao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText edNome;
    private EditText edIdade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edNome = findViewById(R.id.edNome);
        edIdade = findViewById(R.id.edIdade);
    }

    public void okClick(View view) {
        String nome = edNome.getText().toString();
        int idade = Integer.parseInt( edIdade.getText().toString() );

        Toast.makeText(this,
                "Bem Vindo, " + nome + "\nsua idade é " + idade,
                 Toast.LENGTH_LONG).show();

        edNome.setText("");
        edIdade.setText("");
        edNome.requestFocus();
    }
}
