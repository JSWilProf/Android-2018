package br.senai.sp.informatica.albunsmusicais.lib;

import android.os.AsyncTask;

public class Action<Tipo> extends AsyncTask<Tipo, Void, Void> {
    private DatabaseAction<Tipo> action;

    public Action(DatabaseAction<Tipo> action) {
        this.action = action;
    }

    protected Void doInBackground(Tipo... objs) {
        if(objs instanceof Void[]) {
            action.doAction(null);
        } else {
            action.doAction(objs[0]);
        }
        return null;
    }
}