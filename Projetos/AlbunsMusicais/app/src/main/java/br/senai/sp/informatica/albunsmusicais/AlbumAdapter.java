package br.senai.sp.informatica.albunsmusicais;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class AlbumAdapter extends ListAdapter<Album, AlbumAdapter.AlbumViewHolder> {

    private OnAlbumClicked executor;
   private AlbumViewModel viewModel;
    private boolean editar;

    public AlbumAdapter(MainActivity activity, OnAlbumClicked executor) {
        super(new DiffUtil.ItemCallback<Album>() {
            @Override
            public boolean areItemsTheSame(Album oldItem, Album newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(Album oldItem, Album newItem) {
                return oldItem.equals(newItem);
            }
        });

        this.executor = executor;
        viewModel = ViewModelProviders.of(activity).get(AlbumViewModel.class);
    }

    public void setEditar(boolean editar) {
        this.editar = editar;
        notifyDataSetChanged();
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Registrar o layout do item da lista (com a foto)
        LayoutInflater svc = LayoutInflater.from(parent.getContext());
        View layout = svc.inflate(R.layout.item_layout, parent, false);
        return new AlbumViewHolder(layout, executor);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {
        // localizar a informação para uma linha especifica do Recycler View
        Album obj = getItem(position);
        holder.bindTo(obj);
    }

    // Responsável por inserir os dados no layout para cada linha
    public class AlbumViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView idCapa;
        private TextView tvBanda;
        private TextView tvAlbum;
        private TextView tvEstilo;
        private TextView tvLancamento;
        private CheckBox chDel;
        private View view;
        private OnAlbumClicked executor;

        public AlbumViewHolder(View itemView, OnAlbumClicked executor) {
            super(itemView);
            this.executor = executor;

            // obter a referência dos campos do layout
            view = itemView;
            idCapa = view.findViewById(R.id.idCapa);
            tvBanda = view.findViewById(R.id.tvBanda);
            tvAlbum = view.findViewById(R.id.tvAlbum);
            tvEstilo = view.findViewById(R.id.tvEstilo);
            tvLancamento = view.findViewById(R.id.tvLancamento);
            //NOVO
            chDel = view.findViewById(R.id.checkBox);
            //FIM
        }

        public void bindTo(Album obj) {
            tvBanda.setText(obj.getBanda());
            tvAlbum.setText(obj.getAlbum());
            tvEstilo.setText(obj.getEstilo());
            tvLancamento.setText(obj.getDataDeLancamento());

            carregaFoto(idCapa, obj.getCapa(), obj.getBanda());

            chDel.setChecked(obj.isDel());
            chDel.setTag(obj.getId());
            chDel.setOnClickListener(this);

            if(editar) {
                chDel.setVisibility(View.VISIBLE);
            } else {
                chDel.setVisibility(View.GONE);
            }

            view.setOnClickListener(this);
            view.setTag(obj.getId());
        }

        @Override
        public void onClick(View view) {
            //NOVO
            Long id = (Long)view.getTag();

            if(view instanceof CheckBox) {
                Album album = viewModel.localizar(id);
                album.setDel(!album.isDel());
                viewModel.atualizar(album);
            } else {
                // solicitar a abertura da Activity de Edição para o id selecionado
                executor.editarAlbum(id);
            }
            //FIM

        }

        private void carregaFoto(ImageView capa, byte[] foto, String nomeBanda) {
            if(foto != null) {
                // Transforma o vetor de bytes de base64 para bitmap
                Bitmap bitmap = Utilitarios.bitmapFromBase64(foto);
                // Cria uma foto circular e atribui à foto
                capa.setImageBitmap(bitmap);
            } else {
                // Obtem a 1ª letra do nome da pessoa e converte para Maiuscula
                String letra = nomeBanda.substring(0, 1).toUpperCase();
                // Cria um bitmap contendo a letra
                Bitmap bitmap = Utilitarios.circularBitmapAndText(
                        Color.parseColor("#936A4D"), 200, 200, letra);
                // atribui à foto
                capa.setBackgroundColor(Color.TRANSPARENT);
                capa.setImageBitmap(bitmap);
            }
        }
    }
}
