package br.senai.sp.informatica.albunsmusicais.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import br.senai.sp.informatica.albunsmusicais.Main;
import br.senai.sp.informatica.albunsmusicais.R;

//TODO: Criar a classe para obtera informação de ordenação
public class PreferenceUtil {
    public static String getOrdem() {
        Context ctx = Main.getContext();
        // Obtém a identificação da preferência para Ordenação
        String ordemPreference = ctx.getResources().getString(R.string.ordem_key);
        // Obtém o valor padrão para a Ordenação
        String ordemDefault = ctx.getResources().getString(R.string.ordem_default);
        // Obtém o recurso de leitura de preferências
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        // Localiza a configuração selecionada para Ordenação de Albuns
        return preferences.getString(ordemPreference, ordemDefault);
    }
}
