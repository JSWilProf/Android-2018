package br.senai.sp.informatica.albunsmusicais.view;

public interface OnAlbumClicked {
    public void editarAlbum(Long id);
}
