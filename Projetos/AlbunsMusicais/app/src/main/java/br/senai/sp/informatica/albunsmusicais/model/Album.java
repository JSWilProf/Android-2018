package br.senai.sp.informatica.albunsmusicais.model;

import android.annotation.SuppressLint;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Album {
    @PrimaryKey
    private Long id;
    private String banda;
    private String album;
    private String estilo;
    private Date lancamento;
    private byte[] capa;
    private boolean del;

    @SuppressLint("SimpleDateFormat")
    private static SimpleDateFormat fmtData =
            new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");

    public String getDataDeLancamento() {
        return fmtData.format(lancamento);
    }

    @Override
    public Album clone() {
        return Album.builder()
                .id(id)
                .album(new String(album))
                .banda(new String(banda))
                .estilo(new String(estilo))
                .capa(capa == null ? capa : new String(capa).getBytes())
                .del(del)
                .lancamento(new Date(lancamento.getTime()))
                .build();
    }
}
