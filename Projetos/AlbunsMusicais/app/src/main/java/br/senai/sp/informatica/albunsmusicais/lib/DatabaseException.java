package br.senai.sp.informatica.albunsmusicais.lib;

public class DatabaseException extends Exception {
    public DatabaseException(String message) {
        super(message);
    }
}
