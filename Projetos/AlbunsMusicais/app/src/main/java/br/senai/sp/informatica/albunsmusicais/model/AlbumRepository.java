package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import java.util.concurrent.ExecutionException;

import br.senai.sp.informatica.albunsmusicais.lib.Action;
import br.senai.sp.informatica.albunsmusicais.lib.DatabaseException;
import br.senai.sp.informatica.albunsmusicais.lib.Query;
import br.senai.sp.informatica.albunsmusicais.view.PreferenceUtil;

public class AlbumRepository {
    private AlbumDao albumDao;
    private LiveData< PagedList<Album> > albuns;

    public AlbumRepository() {
        AlbumDatabase db = AlbumDatabase.getInstance();
        albumDao = db.albumDao();

        inicializa();
    }

    public void inicializa() {
        DataSource.Factory<Integer, Album> factory;
        String ordem = PreferenceUtil.getOrdem();

        switch (ordem) {
            case "Banda":
                factory = albumDao.getAlbunsPorBanda();
                break;
            case "Album":
                factory = albumDao.getAlbunsPorAlbum();
                break;
            default:
                factory = albumDao.getAlbunsPorLancamento();
        }

        albuns = new LivePagedListBuilder(factory, 10).build();
    }

    public LiveData<PagedList<Album>> getAlbuns() {
        return albuns;
    }

    public void salvar(Album album) {
        new Action<Album>( obj -> albumDao.salvar(obj) ).execute(album);
    }

    public void inserir(Album album) {
        new Action<Album>( obj -> albumDao.inserir(obj) ).execute(album);
    }

    public Album localizar(long id) throws DatabaseException {
        try {
            return new Query<Long, Album>( chave -> albumDao.localizar(chave) ).execute(id).get();
        } catch (ExecutionException | InterruptedException ex) {
            throw new DatabaseException("Falha na execução da consulta do ID: " + id);
        }
    }

    public void removerMarcados() {
        new Action<Void>( id -> albumDao.removerMarcados() ).execute();
    }

    public int existeAlbunsADeletar() throws DatabaseException {
        try {
            return new Query<Void, Integer>( valor -> albumDao.existeAlbunsADeletar() ).execute().get();
        } catch (ExecutionException | InterruptedException ex) {
            throw new DatabaseException("Falha em determinar se existem Albuns à remover");
        }
    }

    public void limpaMarcados() {
        new Action<Void>( valor -> albumDao.limpaMarcados()).execute();
    }

    public int existeAlbuns() throws DatabaseException {
        try {
            return new Query<Void, Integer>( valor -> albumDao.existeAlbuns() ).execute().get();
        } catch (ExecutionException | InterruptedException ex) {
            throw new DatabaseException("Falha em determinar se existem Albuns");
        }
    }

}
