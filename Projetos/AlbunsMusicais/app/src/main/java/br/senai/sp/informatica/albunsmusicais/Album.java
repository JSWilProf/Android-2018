package br.senai.sp.informatica.albunsmusicais;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Album {
    private Long id;
    private String banda;
    private String album;
    private String estilo;
    private Date lancamento;
    private byte[] capa;
    private boolean del;

    @SuppressLint("SimpleDateFormat")
    private static SimpleDateFormat fmtData =
            new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");

    public String getDataDeLancamento() {
        return fmtData.format(lancamento);
    }

    @Override
    public Album clone() {
        return Album.builder()
                .id(id)
                .album(new String(album))
                .banda(new String(banda))
                .estilo(new String(estilo))
                .capa(capa == null ? capa : new String(capa).getBytes())
                .del(del)
                .lancamento(new Date(lancamento.getTime()))
                .build();
    }
}
