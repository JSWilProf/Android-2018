package br.senai.sp.informatica.albunsmusicais;

public interface OnAlbumClicked {
    public void editarAlbum(Long id);
}
