package br.senai.sp.informatica.albunsmusicais.lib;

import android.widget.Toast;

import br.senai.sp.informatica.albunsmusicais.Main;

public class Alerta {
    public static void show(String mensagem) {
        Toast.makeText(Main.getContext(), mensagem, Toast.LENGTH_LONG).show();
    }
}
