package br.senai.sp.informatica.albunsmusicais;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

public class AlbumViewModel extends ViewModel implements PropertyChangeListener {
    private AlbumDao dao = AlbumDao.instance;
    public MutableLiveData<List<Album>> albumList;

    public LiveData<List<Album>> getAlbumList() {
        if(albumList == null) {
            albumList = new MutableLiveData<>();
            albumList.postValue(dao.getAlbuns());
            dao.addListener(this);
        }
        return albumList;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        albumList.postValue(dao.getAlbuns());
    }

    public void inserir(Album album) {
        dao.inserir(album);
    }

    public void atualizar(Album album) {
        dao.salvar(album);
    }

    public Album localizar(long id) {
        return dao.localizar(id);
    }

    public void remover(long id) {
        dao.remover(id);
    }

    public void removerMarcados() {
        dao.removerMarcados();
    }

    public boolean existeAlbunsADeletar() {
        return dao.existeAlbunsADeletar();
    }

    public void limpaMarcados() {
        dao.limpaMarcados();
    }
}
