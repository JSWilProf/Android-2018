package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import br.senai.sp.informatica.albunsmusicais.Main;
import br.senai.sp.informatica.albunsmusicais.lib.DateTypeConverter;

@Database(entities = {Album.class}, version = 1, exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class AlbumDatabase extends RoomDatabase {
    private static AlbumDatabase instance;

    public abstract AlbumDao albumDao();

    static AlbumDatabase getInstance() {
        if(instance == null) {
            instance = Room.databaseBuilder(Main.getContext(),AlbumDatabase.class, "albumdb")
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return instance;
    }
}
