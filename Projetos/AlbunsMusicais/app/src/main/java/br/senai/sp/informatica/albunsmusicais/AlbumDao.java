package br.senai.sp.informatica.albunsmusicais;

import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ListAdapter;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

public class AlbumDao {
    public static final AlbumDao instance = new AlbumDao();
    private static List<Album> lista = new ArrayList<>();
    private static long id = 1L;

    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    // Executado quando a Classe AlbumDao é carregada na memória, isto ocorre uma única vez.
    static {
       lista.add(new Album(0L, "AC/DC", "Rock or Burst", "Rock",
                new GregorianCalendar(2014, 10, 1).getTime(),
                null, false));
    }

    public Album localizar(long id) {
        return this.localizar(id, true);
    }

    private Album localizar(long id, boolean clonar) {
        for(Album album : lista) {
            if(album.getId() == id) {
                return clonar ? album.clone() : album;
            }
        }
        return null;
    }


    public void salvar(Album obj) {
        Album item = localizar(obj.getId(), false);
        Album velho = item.clone();

        if(item != null) {
            item.setBanda(obj.getBanda());
            item.setAlbum(obj.getAlbum());
            item.setEstilo(obj.getEstilo());
            item.setLancamento(obj.getLancamento());
            item.setCapa(obj.getCapa());
            item.setDel(obj.isDel());

            changeSupport.firePropertyChange("salvaAlbum", velho, item);
        }
    }

    public void inserir(Album obj) {
        obj.setId(id++);
        lista.add(obj);
        changeSupport.firePropertyChange("novoAlbum",null, obj);
    }

    public void remover(long id) {
        Album album = localizar(id, false);
        lista.remove(album);
        changeSupport.firePropertyChange("apagaAlbum",album, null);
    }

    public List<Album> getAlbuns() {
        List<Album> osAlbuns = new ArrayList<>(lista.size());
        for(Album album : lista) {
            osAlbuns.add(album.clone());
        }
        return osAlbuns;
    }

    public void removerMarcados() {
        List<Album> osAlbuns = new ArrayList<>();
        for(Album album : lista) {
            if(album.isDel())
                osAlbuns.add(album);
        }

        for(Album album : osAlbuns) {
            remover(album.getId());
        }
    }

    public boolean existeAlbunsADeletar() {
        boolean existe = false;
        for(Album album : lista) {
            if(album.isDel()) {
                existe = true;
                break;
            }
        }
        return existe;
    }

    public void limpaMarcados() {
        for(Album album : lista) {
            if(album.isDel()) {
                Album novo = album.clone();
                novo.setDel(false);
                salvar(novo);
            }
        }
    }

    public void addListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

}