package br.senai.sp.informatica.albunsmusicais;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.app.ActionBar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.InputDeviceCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressLint("SimpleDateFormat")
public class EditarActivity extends AppCompatActivity {
    private TextInputEditText edBanda;
    private TextInputEditText edAlbum;
    private TextInputEditText edGenero;
    private TextInputEditText edLancamento;
    private Button btLancamento;
    private ImageView idCapa;
    private SimpleDateFormat fmtData =
            new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");
    private Calendar calendar = Calendar.getInstance();
    private Album album;
    private AlbumViewModel viewModel;
    private Uri imageURI;

    private static final int REQUEST_IMAGE_GALERY = 0;
    private static final int REQUEST_GALERY_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        edAlbum = findViewById(R.id.edAlbum);
        edBanda = findViewById(R.id.edBanda);
        edGenero = findViewById(R.id.edGenero);
        edLancamento = findViewById(R.id.edLancamento);
        edLancamento.setText(fmtData.format(new Date()));

        btLancamento = findViewById(R.id.btLancamento);
        idCapa = findViewById(R.id.idCapa);

        // Obter as informações do Album e
        // atualizar a tela

        viewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            long id = extra.getLong("id");
            // utilizar o ID para localizar o Album
            album = viewModel.localizar(id);
            if(album != null) {
                edBanda.setText(album.getBanda());
                edAlbum.setText(album.getAlbum());
                edGenero.setText(album.getEstilo());
                edLancamento.setText(album.getDataDeLancamento());

                calendar.setTime(album.getLancamento());

                byte[] foto = album.getCapa();
                if(foto != null) {
                    Bitmap bitmap = Utilitarios.bitmapFromBase64(foto);
                    idCapa.setImageBitmap(bitmap);
                }
            }
        }

        ActionBar bar = getActionBar();
        if(bar != null) {
            bar.setHomeButtonEnabled(true);
            bar.setDisplayHomeAsUpEnabled(true);
        }

        Utilitarios.hideKeyboard(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED);
        } else if(id == R.id.ac_salvar) {

            boolean editar = true;

            if(album == null) {
                album = new Album();
                editar = false;
            }

            album.setBanda(edBanda.getText().toString());
            album.setAlbum(edAlbum.getText().toString());
            album.setEstilo(edGenero.getText().toString());
            album.setLancamento(calendar.getTime());

            Bitmap bitmap = Utilitarios.bitmapFromImageView(idCapa);
            if(bitmap != null) {
                album.setCapa(Utilitarios.bitmapToBase64(bitmap));
            } else {
                album.setCapa(null);
            }

            if(editar) {
                viewModel.atualizar(album);
            } else {
                viewModel.inserir(album);
            }

            setResult(Activity.RESULT_OK);
        }

        finish();

        return true;
    }

    public void onDateClicked(View view) {
        DateDialog.makeDialog(calendar, edLancamento)
                .show(getFragmentManager(), "");
    }

    public void onCapaClicked(View view) {
        abrirGaleria();
    }

    private void abrirGaleria() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        if (intent.resolveActivity(getPackageManager()) != null) {
            if ((ContextCompat.checkSelfPermission(getBaseContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_GALERY_PERMISSION);
            } else {
                startActivityForResult(Intent.createChooser(intent, "Selecione a Foto"),
                        REQUEST_IMAGE_GALERY);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean autorizado = true;

        for(int resultato : grantResults) {
            if(resultato == PackageManager.PERMISSION_DENIED) {
                autorizado = false;
                break;
            }
        }

        switch (requestCode) {
            case REQUEST_GALERY_PERMISSION:
                if(autorizado)
                    abrirGaleria();
                else
                    Toast.makeText(this, "O Acesso à Galeria de Fotos foi negado!", Toast.LENGTH_LONG).show();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_GALERY) { // Recebe a Foto da Galeria de Fotos
            if(data != null) {
                try {
                    imageURI = data.getData();

                    Bitmap bitmap = Utilitarios.setPic(idCapa.getWidth(), idCapa.getHeight(), imageURI, this);
                    idCapa.setImageBitmap(bitmap);
                    idCapa.invalidate();
                } catch (IOException ex) {
                    Toast.makeText(this, "Falha ao abrir a Foto", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(imageURI != null) {
            outState.putParcelable("uri", imageURI);
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        imageURI = savedInstanceState.getParcelable("uri");
        carregaCapa(imageURI);
    }

    private void carregaCapa(Uri imageURI) {
        try {
            if (imageURI != null) {
                Bitmap bitmap = Utilitarios.setPic(idCapa.getWidth(), idCapa.getHeight(), imageURI, this);
                idCapa.setImageBitmap(bitmap);
                idCapa.invalidate();
            } else {
                if(album != null) {
                    byte[] capabase64 = album.getCapa();
                    if (capabase64 != null) {
                        Bitmap bitmap = Utilitarios.bitmapFromBase64(capabase64);
                        idCapa.setImageBitmap(bitmap);
                        idCapa.invalidate();
                    }
                }
            }
        } catch (IOException ex) {
            Toast.makeText(this, "Falha ao abrir a Foto", Toast.LENGTH_LONG).show();
        }
    }
}
