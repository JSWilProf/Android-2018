package br.senai.sp.informatica.albunsmusicais.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.senai.sp.informatica.albunsmusicais.R;
import br.senai.sp.informatica.albunsmusicais.lib.Alerta;
import br.senai.sp.informatica.albunsmusicais.lib.DatabaseException;
import br.senai.sp.informatica.albunsmusicais.model.AlbumViewModel;

public class MainActivity extends AppCompatActivity  implements OnAlbumClicked {
    private LinearLayoutManager layoutManager;
    private AlbumAdapter adapter;
    private MenuItem idEdit;
    private MenuItem idDelete;
    private AlbumViewModel viewModel;
    private boolean editar;
    private String ordem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new AlbumAdapter(this, this);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);
        viewModel.getAlbumList().observe(this, albums -> {
                adapter.submitList(albums);
            });

        FloatingActionButton btAdicionar = findViewById(R.id.btAdcionar);
        btAdicionar.setOnClickListener(v -> {
                // Chamar a Activity de Edição de Albuns
                editarAlbum(null);
            });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_apagar, menu);
        idEdit = menu.findItem(R.id.ic_editar);
        idDelete = menu.findItem(R.id.ic_apagar);
        setEditar(editar);

        return true;
    }

    @Override
    public void editarAlbum(Long id) {
        Intent tela = new Intent(this, EditarActivity.class);
        if(id != null) {
            tela.putExtra("id", id);
        }
        startActivity(tela); //ForResult(tela, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        try {
            if(id == R.id.ic_editar) {
                if (viewModel.existeAlbuns()) {
                    setEditar(true);
                }
            } else if(id == R.id.ic_pref) {
                Intent pref = new Intent(this, PreferenciasActivity.class);
                ordem = PreferenceUtil.getOrdem();
                startActivityForResult(pref, 0);
            } else { // R.id.ic_apagar
                    if (viewModel.existeAlbunsADeletar()) {
                        AlertDialog.Builder alerta = new AlertDialog.Builder(this);
                        alerta.setMessage("Confirma a exclusão deste(s) Album(ns)?");

                        alerta.setNegativeButton("Não", (dialogo, botao) -> viewModel.limpaMarcados());
                        alerta.setPositiveButton("Sim", (dialogo, botao) -> viewModel.removerMarcados());
                        alerta.create();
                        alerta.show();
                    }
                    setEditar(false);
            }
        } catch (DatabaseException ex) {
            Alerta.show(ex.getMessage());
        }

        return true;
    }

    private void setEditar(boolean valor) {
        editar = valor;
        adapter.setEditar(valor);
        idEdit.setVisible(!valor);
        idDelete.setVisible(valor);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(!ordem.equals(PreferenceUtil.getOrdem())) {
            viewModel.getAlbumList().removeObservers(this);
            viewModel.mudouAOrdem();
            viewModel.getAlbumList().observe(this, albuns -> adapter.submitList(albuns));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("editar", editar);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        editar = savedInstanceState.getBoolean("editar");
    }
}

