package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.paging.DataSource;
import android.arch.persistence.room.*;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

@Dao
public interface AlbumDao {
    @Query("select * from album where id = :id")
    public Album localizar(long id);

    @Update
    public void salvar(Album obj);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void inserir(Album obj);

    @Query("delete from album where del = '1'")
    public void removerMarcados();

    @Query("select count(*) from  album where del = '1'")
    public int existeAlbunsADeletar();

    @Query("update album set del = '0' where del = '1'")
    public void limpaMarcados();

    @Query("select count(*) from album")
    public int existeAlbuns();

    @Query("select * from album order by banda")
    public DataSource.Factory<Integer, Album> getAlbunsPorBanda();

    @Query("select * from album order by album")
    public DataSource.Factory<Integer, Album> getAlbunsPorAlbum();

    @Query("select * from album order by lancamento")
    public DataSource.Factory<Integer, Album> getAlbunsPorLancamento();
}